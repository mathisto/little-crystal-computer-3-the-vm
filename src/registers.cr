module Registers
  extend self

  # `R` contains the 10 registers of the LC-3. Each register is an 16 bits. R0-R7 are
  # general purpose. PC is the _program counter_. COND is the condition
  # flag
  enum R : UInt16
    R0    # 0000b 0x0 0
    R1    # 0001b 0x1 1
    R2    # 0010b 0x2 2
    R3    # 0011b 0x3 3
    R4    # 0100b 0x4 4
    R5    # 0101b 0x5 5
    R6    # 0110b 0x6 6
    R7    # 0111b 0x7 7
    PC    # 1000b 0x8 8
    COND  # 1001b 0x9 9
    COUNT # 1010b 0xA 10
  end
end
