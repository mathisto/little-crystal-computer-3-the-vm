module OpCodes
  extend self

  # The LC-3 Instruction Set (OP Codes)
  enum OP : UInt16
    BR    # branch                  0000b 0x0 0
    ADD   # add                     0001b 0x1 1
    LD    # load                    0010b 0x2 2
    ST    # store                   0011b 0x3 3
    JSR   # jump sub-routine        0100b 0x4 4
    AND   # bitwise and             0101b 0x5 5
    LDR   # load register           0110b 0x6 6
    STR   # store register          0111b 0x7 7
    RTI   # unused                  1000b 0x8 8
    NOT   # bitwise not             1001b 0x9 9
    LDI   # load indirect           1010b 0xA 10
    STI   # store indirect          1011b 0xB 11
    JMP   # jump                    1100b 0xC 12
    RES   # reserved (unused)       1101b 0xD 13
    LEA   # load effective address  1110b 0xE 14
    TRAP  # execute trap            1111b 0xF 15
  end
end
