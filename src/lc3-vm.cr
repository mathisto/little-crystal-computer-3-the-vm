require "option_parser"

require 'op_codes'
require 'registers'
require 'flags'
require 'operations'

# A crystal implementation of the LC-3 architecture.
module LC3
  VERSION = "0.0.420"
  U_INT16_MAX = 65_536
  PC_START = 0x3000_u16

  include OpCodes
  include Registers
  include Flags
  include Operations

  memory = Array(UInt16).new(U_INT16_MAX, 0) # 65,536 locations.
  reg = Array(UInt16).new(R::COUNT, 0) # array of registers
  reg[R::COND] = FL::ZRO # Initialize the condition register to zero
  reg[R::PC] = PC_START # Initialize the program counter to the start of the program

  # Handle CLI options
  OptionParser.parse do |parser|
    parser.banner = "A crystal implementation of a LC-3 VM"

    parser.on "-v", "--version", "Show version" do
      puts "version #{VERSION}"
      exit
    end
    parser.on "-h", "--help", "Show help" do
      puts parser
      exit
    end
  end

  running = true
  while running
      # Fetches the next instruction from memory.
    instruction = memory[reg[R::PC]++]
    operation = instruction >> 12

    case operation
    in .BR? branch; break
    in .ADD? add; break
    in .LD? ld; break
    in .ST? store; break
    in .JSR? jump_sub_routine; break
    in .AND? and; break
    in .LDR? load_register; break
    in .STR? store_register; break
    in .RTI? raise "Unused Op Code: #{operation}"
    in .NOT? not; break
    in .LDI? load_indirect; break
    in .STI? store_indirect; break
    in .JMP? jump; break
    in .RES? raise "Unused Op Code: #{operation}"
    in .LEA? load_effective_address; break
    in .TRAP? trap; break
    else raise "Bad Op Code: #{operation}"
    end
  end

  # Shutdown and clean up.
end
