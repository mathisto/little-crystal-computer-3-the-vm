module Helpers
  extend self

  # The immediate mode value has only 5 bits, but it needs to be added to a 16-bit number.
  # To do the addition, those 5 bits need to be extended to 16 to match the other number.
  # For positive numbers, we can simply fill in 0's for the additional bits.
  # For negative numbers, this causes a problem. For example, -1 in 5 bits is 1 1111.
  # If we just extended it with 0's, this is 0000 0000 0001 1111 which is equal to 31.
  # Sign extension corrects this problem by filling in 0's for positive numbers and 1's
  # for negative numbers, so that original values are preserved.
  def sign_extend(x : UInt16, bit_count : UInt8)
    if ((x >> (bit_count - 1)) & 1).zero?
      x |= (0xFFFF << bit_count) # Binary OR assignment - flood the rest with 1's
    end
  end

  def zero_extend()
    # TODO: Implement me
  end

  def memory_read(address : UInt16)
    #
  end

  # Convienience slicer described as notation in the LC-3 ISA.
  # The field delimited by bit [l] on the left and bit [r] on the right, of the datum A. For
  # example, if PC contains 0011001100111111, then PC[15:9] is 0011001. PC[2:2]
  # is 1. If l and r are the same bit number, the notation is usually abbreviated PC[2].
  def bit_field(left : UInt8, right : UInt8)
    # Implement me please
  end
end
