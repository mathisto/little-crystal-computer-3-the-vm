module Operations
  extend self

  # The condition codes specified by the state of bits [11:9] are tested. If bit [11] is
  # set, N is tested; if bit [11] is clear, N is not tested. If bit [10] is set, Z is tested, etc.
  # If any of the condition codes tested is set, the program branches to the location
  # specified by adding the sign-extended PCoffset9 field to the incremented PC
  def branch # BR - 0000b 0x0 0
    neg = instruction.bit(11)
    zro = instruction.bit(10)
    pos = instruction.bit(9)
    pc_offset : UInt16 = sign_extend(instruction & 0x1FF, 9)

    if ((n & reg[R::COND].value) OR (z & reg[R::COND].value) OR (p & reg[R::COND].value))
      reg[R::PC] += pc_offset
    end
  end

  # If bit [5] is 0, the second source operand is obtained from SR2. If bit [5] is 1, the
  # second source operand is obtained by sign-extending the imm5 field to 16 bits.
  # In both cases, the second source operand is added to the contents of SR1 and the
  # result stored in DR. The condition codes are set, based on whether the result is
  # negative, zero, or positive.
  #
  # *Assembler Formats*
  # `ADD DR, SR1, SR2`
  # `ADD DR, SR1, imm5`
  # *Examples*
  # `ADD R2, R3, R4 ; R2 ← R3 + R4`
  # `ADD R2, R3, #7 ; R2 ← R3 + 7`
  def add  # ADD - 0001b 0x1 1
    destination_register : UInt16 = (instruction >> 9) & 0x7
    source_register_1 : UInt16 = (instruction >> 6) & 0x7
    immediate_flag : UInt16 = (instruction >> 5) & 0x1

    if immediate_flag
      immediate_value : UInt16 = sign_extend(instruction & 0x1F, 5)
      reg[destination_register] = reg[source_register_1] + immediate_value
    else
      source_register_2 : UInt16 = (instruction >> 0) & 0x7
      reg[destination_register] = reg[source_register_1] + reg[source_register_2]
    end

    update_flags(destination_register)
  end

  # An address is computed by sign-extending bits [8:0] to 16 bits and adding this
  # value to the incremented PC. The contents of memory at this address are loaded
  # into DR. The condition codes are set, based on whether the value loaded is
  # negative, zero, or positive.
  def load # LD - 0b0010 0x2 2
    destination_register : UInt16 = (instruction >> 9) & 0x7
    pc_offset : UInt16 = sign_extend(instruction & 0x1FF, 9)

    reg[destination_register] = memory[R::PC + pc_offset]

    update_flags(destination_register)
  end

  # The contents of the register specified by SR are stored in the memory location
  # whose address is computed by sign-extending bits [8:0] to 16 bits and adding this
  # value to the incremented PC.
  def store # ST - 0b0011 0x3 3
    source_register : UInt16 = (instruction >> 9) & 0x7
    pc_offset : UInt16 = sign_extend(instruction & 0x1FF, 9)

    memory[R::PC + sign_extend(pc_offset_9)] = source_register
  end

  # First, the incremented PC is saved in R7. This is the linkage back to the calling
  # routine. Then the PC is loaded with the address of the first instruction of the
  # subroutine, causing an unconditional jump to that address. The address of the
  # subroutine is obtained from the base register (if bit [11] is 0), or the address is
  # computed by sign-extending bits [10:0] and adding this value to the incremented
  # PC (if bit [11] is 1).
  def jump_sub_routine # JSR[R] - 0b0100 0x4 4
    register[R::R7] = R::PC
    offset_flag : Boolean = instruction.bit(11) == 1

    if offset_flag
      pc_offset_11 : UInt16 = sign_extend(instruction.bits(0..10))
      register[R::PC] += pc_offset_11
    else
      base_register : UInt16 = instruction.bits(6..8)
      register[R::PC] = base_register
    end
  end

  # If bit [5] is 0, the second source operand is obtained from SR2. If bit [5] is 1,
  # the second source operand is obtained by sign-extending the imm5 field to 16
  # bits. In either case, the second source operand and the contents of SR1 are bitwise ANDed, and the result stored in DR. The condition codes are set, based on
  # whether the binary value produced, taken as a 2’s complement integer, is negative,
  # zero, or positive.
  def and # AND - 0b0101 0x5 5
    destination_register : UInt16 = instruction.bits(9..11)
    source_register_1 : UInt16 = instruction.bits(6..8)
    immediate_flag : UInt16 = instruction.bit(5)

    if immediate_flag
      immediate_value : UInt16 = sign_extend(instruction.bits(0..4))
      reg[destination_register] = reg[source_register_1] & immediate_value
    else
      source_register_2 : UInt16 = instruction.bits(0..2)
      reg[destination_register] = reg[source_register_1] & reg[source_register_2]
    end

    update_flags(destination_register)
  end

  # An address is computed by sign-extending bits [5:0] to 16 bits and adding this
  # value to the contents of the register specified by bits [8:6]. The contents of memory
  # at this address are loaded into DR. The condition codes are set, based on whether
  # the value loaded is negative, zero, or positive.
  def load_register # LDR 0b0110 0x6 6
    destination_register : UInt16 = instruction.bits(9..11)
    base_register : UInt16 = instruction.bits(6..8)
    offset_6 : UInt16 = sign_extend(instruction.bits(0..5))

    reg[destination_register] = memory[base_register + offset_6]

    update_flags(destination_register)
  end

  # The contents of the register specified by SR are stored in the memory location
  # whose address is computed by sign-extending bits [5:0] to 16 bits and adding this
  # value to the contents of the register specified by bits [8:6].
  def store_register # STR - 0b0111 0x7 7
    store_register : UInt16 = instruction.bits(9..11)
    base_register : UInt16 = instruction.bits(6..8)
    offset_6 : UInt16 = sign_extend(instruction.bits(0..5))

    memory[base_register + offset_6] = store_register

    update_flags(destination_register)
  end

  # The bit-wise complement of the contents of SR is stored in DR. The condition codes are set, based on whether the binary value produced, taken as a 2’s
  # complement integer, is negative, zero, or positive.
  def not # NOT - 0b1001 0x9 9
    destination_register : UInt16 = instruction.bits(9..11)
    source_register : UInt16 = instruction.bits(6..8)

    register[destination_register] = ~source_register

    update_flags(destination_register)
  end

  # An address is computed by sign-extending bits [8:0] to 16 bits and adding this
  # value to the incremented PC. What is stored in memory at this address is the
  # address of the data to be loaded into DR. The condition codes are set, based on
  # whether the value loaded is negative, zero, or positive.
  def load_indirect # LDI 0b1010 0xA 10
    destination_register : UInt16 = (instruction >> 9) & 0x7
    pc_offset : UInt16 = sign_extend(instruction & 0x1FF, 9)

    reg[destination_register] = memory_read(memory_read(reg[R::PC] + pc_offset))

    update_flags(destination_register)
  end

  # The contents of the register specified by SR are stored in the memory location
  # whose address is obtained as follows: Bits [8:0] are sign-extended to 16 bits and
  # added to the incremented PC. What is in memory at this address is the address of
  # the location to which the data in SR is stored.
  def store_indirect # STI - 0b1011 0xB 11
    source_register : UInt16 = (instruction >> 9) & 0x7
    pc_offset : UInt16 = sign_extend(instruction & 0x1FF, 9)

    reg[destination_register] = memory_read(memory_read(reg[R::PC] + pc_offset))

  end
end
