module Flags
  extend self

  # The flag values for the R::COND register
  enum FL
    POS = 1 << 0
    ZRO = 1 << 1
    NEG = 1 << 2
  end

  # Any time a value is written to a register, we need to update the flags to indicate its sign.
  def update_flags(r : UInt16)
    return reg[R::COND] = FL::ZRO.value if ref[r].zero?
    return reg[R::COND] = FL::NEG.value if ref[r] >> 15 != 0x00
    reg[R::COND] = FL::POS.value
  end
end
