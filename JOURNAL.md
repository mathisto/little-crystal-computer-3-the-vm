# Day 1 - Sunday, February 6, 2022
- Setup repo for crystal implementation of the LC-3 VM
- Create Op Codes, Registers, Flags, and Instructions
- Flesh out README with a semblance of a plan
- Iniital read-through of JonesForth and ArpiLisp literate docs

# Day 2 - Monday, February 7, 2022
- Read through buzzard FIRST and THIRD
- Implement LDI (load indirect)
- Implement BR (branch)
- Implement LD (load)
- Fix silly error in ADD
- Read up and play with bitwise operators.

# Day 3 - Tuesday, February 8, 2022
- Added additional tutorial help to README
- More playtime with bit shifts.
- Read Sean Eron Anderson's ["Bit Twiddling Hacks"](http://graphics.stanford.edu/~seander/bithacks.html)
- Started a crystal docs folder for relevant learnings
- Jammed with brian discussing bit manipulation

# Day 4 - Wednesday, February 9, 2022
- Implement ST (store)
- Implement JSR (jump sub-routine)
- Implement AND (or, j/k, and)
- Implement LDR (load register)
- Implement STR (store register)
- Implement NOT (nawt, lol)
- Implement STI (store indirect)
- Implement JMP (jump)
- Implement LEA (load effective address)
- Implement TRAP (claptrap)
#### TODO:
  - Device Registers
  - Trap Service Routines
  - Implment `Helpers.zero_extend`
  - Implement `Helpers.memory_read`
  - Check consistency/proper usage of `R::PC` vs `R::PC.value` for all `R`
  - Check consistency/proper usage of `memory[i]` vs `memory_read`
  - Finish CLI arg handling to accept binary/asm files
