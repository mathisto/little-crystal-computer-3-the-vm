# [Little Crystal Computer 3 - The VM](https://gitlab.com/mathisto/little-crystal-computer-3-the-vm):
## Ex Nihilo to VM to FORTH to Lisp.

I'm learning how magical computers are by demystifying them. I'm going to dive straight to the deepest circles of hell (Assembly Language) and then try to pull myself up by my own bootstraps to fly to the gates of heaven (Lisp).

## The Plan:
  1. Implement a "rich, but lean" VM (The [LC-3 Architecture](https://en.wikipedia.org/wiki/Little_Computer_3)) using [Crystal](https://crystal-lang.org). Following along with the sensational tutorial by Justin Meiners and Ryan Pendleton: ["Write your Own Virtual Machine"](https://justinmeiners.github.io/lc3-vm/). More nitty-gritty, low-level details can be found at ["Crafting Interpreters"](https://www.craftinginterpreters.com/a-bytecode-virtual-machine.html).
1. Once in place, touch that low-level ASM as little as possible. Immediatly implement a bare bones Forth. The venerable [literate sourcecode for JonesForth](docs/forth/jonesforth.S) by Richard W.M. Jones will be the basis for this. Additional marrow to be sucked from the bones of the bootstrapped [FIRST and FORTH by buzzard](docs/forth/buzzard.2.design.md). And even more high level help [here](https://blog.asrpo.com/forth_tutorial_part_1). Expect pain in translating to LC3 ASM. This will provide a much more pleasent environment for building out the next phase of the project.
2. Use the LC3_Forth to implement a barebones Lisp using this [awesome literate source tutorial](docs/lisp/arpilisp.S). It is written for Raspberry Pi ARM architecture, so expect hardship and tears. The LC3_Forth should help soothe the wounds.

## Installation
1. Install Crystal: `brew install crystal`
1. Run the VM: `crystal run src/lc3-vm.cr`

## Contributing

1. Fork it (<https://gitlab.com/mathisto/little-crystal-computer-3-the-vm>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors
- [Matt Kelly](https://gitlab.com/mathisto/) - Thief and vagabond. Generally a bad dude.
