Crystal has some handy bit manipulation methods baked into the core
`#>>(count : Int)`
Returns the result of shifting this number's bits count positions to the right.
```crystal
# Positives
8000 >> 1  # => 4000
8000 >> 2  # => 2000
8000 >> 32 # => 0
8000 >> -1 # => 16000

# Negatives
-8000 >> 1 # => -4000
```

`#bit(bIt)`
Returns this number's bitth bit, starting with the least-significant.
```crystal
11.bit(0) # => 1
11.bit(1) # => 1
11.bit(2) # => 0
11.bit(3) # => 1
11.bit(4) # => 0
```

`#bits(range : Range)`
Returns the requested range of bits
```crystal
0b10011.bits(0..1) # => 0b11
0b10011.bits(0..2) # => 0b11
0b10011.bits(0..3) # => 0b11
0b10011.bits(0..4) # => 0b10011
0b10011.bits(0..5) # => 0b10011
0b10011.bits(1..4) # => 0b1001
```

`#bits_set?(mask)`
Returns true if all bits in mask are set on self.
```crystal
0b0110.bits_set?(0b0110) # => true
0b1101.bits_set?(0b0111) # => false
0b1101.bits_set?(0b1100) # => true
```
